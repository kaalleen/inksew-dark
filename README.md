# Inksew
Inksew is an inkscape extension suite, much like InkStitch, with tools to assist projector sewists.

## Inksew Dark Mode
Inksew Dark Mode is the first module to be released for Inksew. It adds a menu item which simplifies the process of adjusting your pattern's colors to make your cut, sew and reference lines more easily visible on dark colored fabric.

## Installation
Click the download button and choose `.zip` format. Navigate to your download directory and unzip, then move both the `inksew-dark` files to your Inkscape user extension directory (open Inkscape, `Edit -> Preferences -> System` to locate this directory/folder if you do not know where it is). Restart Inkscape.

## Using
When ready to project a pattern, via Inkscape, onto dark fabrics, go to the `Extensions` menu, choose `Inksew`, then `Dark Mode`. When done cutting - or if you decide you do not need Dark Mode for the fabric, repeat the process to restore the colors to the pattern's defaults.

### Limitations
This module, currently, makes a few assumptions about the colors used in your pattern file. Updates are planned to provide additional options (if you want to help, see Contributing). There currently is not a keyboard shortcut assigned - an update will be made available soon (MacOS users, will need your help, see contributing).

## Contributing
Instructions coming soon
