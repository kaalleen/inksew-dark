import inkex
from inkex import Color
from lxml import etree

def add_arguments(self, pars):
    pass

def recolor_pattern_paths(self):

    # A dummy SVG containing an inkscape generated filter
    # which, in this creates an RGB colorspace color inversion
    # TODO keep tweaking the filter, to precisely invert all colors,
    # some non-websafe colors are still mathmatically off, but visually close enough
    filter_text = """
    <svg
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:svg="http://www.w3.org/2000/svg">
        <defs>

       <!-- any additional definitions go here -->

    <filter
       style="color-interpolation-filters:sRGB;"
       inkscape:label="Invert"
       id="filter1"
       x="-0.0070008281"
       y="-0.0072505985"
       width="1.0133967"
       height="1.0111751">
      <feColorMatrix
         values="-1 0 0 0 1 0 -1 0 0 1 0 0 -1 0 1 -0.21 -0.72 -0.07 1.7 0 "
         result="color2"
         id="feColorMatrix1" />
    </filter>

    </defs>
    </svg>

    """

    # Use etree to encode the filter text to an etree element
    my_filter_svg = etree.fromstring(f'{filter_text}')

    # Set a custom filter id
    my_filter_id = 'inksew_dark_mode_filter_01'

    # Check if filter already exists in defs
    filter_id_list = []
    for _filter in self.svg.defs.xpath('//svg:filter'):
         filter_id_list.append(_filter.get_id())

    if my_filter_id in filter_id_list:
          pass
    else:
        # Append the filter element to the svg defs
        my_filter = my_filter_svg.findall('.//{http://www.w3.org/2000/svg}filter')[0]
        # Set the id
        my_filter.set('id', my_filter_id)
        self.svg.defs.append(my_filter)

    # Build a url for the id
    my_filter_url = f'url(#{my_filter_id})'

    # apply the filter to selected elements
    # self.svg.xpath automatically selects all paths
    # normal method has user select paths first then
    # use self.svg.selected:
    for item in self.svg.xpath('//svg:path'):
        # Check if we've already applied the filter
        if item.style.get('filter') == my_filter_url:
            # And remove it
            item.style.pop('filter')

        else:
            # Filter not applied, so, apply it
            item.style['filter'] = my_filter_url
    return

def invert_background(self):

    # Begin page inversion
    # Retrive page color from the SVG, pass it to color for proper formatting
    # and assign to variable colorpage
    colorpage = Color(self.svg.namedview.get('pagecolor'))
    # Comes from inkscape provided extension Color -> Negative
    # color_negative.py, adapted
    for i, channel in enumerate(colorpage):
         colorpage[i] = 255 - channel
    # After inverting the page/background color, write it back to
    # the svg
    self.svg.namedview.set('pagecolor', colorpage)
    return

class DarkMode(inkex.EffectExtension):

    def effect(self):
        invert_background(self)
        recolor_pattern_paths(self)

if __name__ == '__main__':
    DarkMode().run()
